class DownloadProgressChannel < ApplicationCable::Channel
  def subscribed
    stream_from "download_order_csv"
  end

  def unsubscribed
  end

  def receive(data)
    # ActionCable.server.broadcast("chat_#{params[:room]}", data)
  end
end
