App.download_progress = App.cable.subscriptions.create {channel: "DownloadProgressChannel", room: "download_order_csv"},
  connected: ->
  	console.log('Connection estabilished', '...')
    # Called when the subscription is ready for use on the server

  disconnected: ->
  	console.log('Connection disconnected')
    # Called when the subscription has been terminated by the server

  received: (data) ->
  	progress = data['progress']
  	url = data['url']
  	buttonText = if (progress == 100) then 'completed' else 'Inprogress'
  	url = if (progress == 100) then url else ''
  	$('#button-state').html(buttonText)
  	$('#progress-bar').html(data['progress'])
  	$('#url').html(url)
    # Called when there's incoming data on the websocket for this channel
