class BroadcasterController < ApplicationController
	def index
		ActionCable.server.broadcast("download_order_csv", {progress: 100, url: ''})
	end	
end
