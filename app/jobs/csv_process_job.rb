require 'csv'

class CsvProcessJob < ApplicationJob

  def perform(*args)
  	user_id = args[0]
  	user = User.find user_id 
  	orders_count = user.orders.count
  	order_doc_job = CsvUserJob.create(user_id: user_id, progress: 0, status: 'inprogress')

  	CSV.open("#{Rails.root}/public/#{user.name}_orders.csv", "a+") do |csv|
  		user.orders.each_with_index do |order, index|
			csv << [order.product_name, order.quantity, order.product_price, order.total, order.created_at]
			order_doc_job.progress = ((index + 1).to_f/orders_count.to_f) * 100
			ActionCable.server.broadcast("download_order_csv", {progress: order_doc_job.progress, url: ''})
			puts order_doc_job.progress
			order_doc_job.save
  		end

  		ActionCable.server.broadcast("download_order_csv", {progress: 100, url: csv.path})
  	end	

  	order_doc_job.status = 'completed'
  	order_doc_job.save
  end
end
