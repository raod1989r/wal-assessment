class CreateCsvUserJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :csv_user_jobs do |t|
      t.integer :progress
      t.integer :user_id
      t.string :status
      t.string :type

      t.timestamps
    end
  end
end
