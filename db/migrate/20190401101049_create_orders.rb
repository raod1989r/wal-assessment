class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.string :product_name
      t.float :product_price
      t.integer :quantity
      t.integer :total

      t.timestamps
    end
  end
end
