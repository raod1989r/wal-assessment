Rails.application.routes.draw do
  get 'download/index'
  root 'download#index'
  get 'broadcaster/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
