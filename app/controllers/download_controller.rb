class DownloadController < ApplicationController
  def index
  	user_id = 1

	if CsvUserJob.exists?(user_id: user_id, status: 'inprogress')
		@button_state = 'disabled'
	else
  		CsvProcessJob.perform_later user_id
  		@button_state = 'processing'
  	end	
  end
end
