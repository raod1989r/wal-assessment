# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

user = User.create(name: Faker::Name.name)
orders = []
20000.times do |num|
	quantity = rand(2..4)
	price = rand(50..200)
	total = quantity * price 

	orders << user.orders.new(product_name: Faker::Food.vegetables,
							product_price: price, quantity: quantity, total: total)
end	

Order.import orders